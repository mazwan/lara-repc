<?php

namespace App\Listeners;

use App\Events\BroadCastMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBroadCastMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BroadCastMessage  $event
     * @return void
     */
    public function handle(BroadCastMessage $event)
    {
        //
    }
}

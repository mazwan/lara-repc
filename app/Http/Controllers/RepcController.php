<?php

namespace App\Http\Controllers;

use App\Repc;
use Illuminate\Http\Request;

class RepcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $repcs = Repc::all();
        return view('repc.index', compact('repcs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Repc  $repc
     * @return \Illuminate\Http\Response
     */
    public function show(Repc $repc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Repc  $repc
     * @return \Illuminate\Http\Response
     */
    public function edit(Repc $repc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Repc  $repc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Repc $repc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Repc  $repc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repc $repc)
    {
        //
    }
}

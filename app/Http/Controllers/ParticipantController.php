<?php

namespace App\Http\Controllers;

use App\Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pools = (is_array($request->cookie('pool')) ? $request->cookie('pool') : []);        
        $participants = Participant::with('repc')
            ->whereIn('id', $pools)
            ->orderBy('updated_at')
            ->paginate(100);

        $focus = $this->focus_field($request);
        $checkout = $this->can_checkout($request);
        return view('participant.index', compact('participants', 'focus', 'checkout'));
    }

    public function search(Request $request)
    {        
        if ($request->has('search')) {
            $participants = Participant::with('repc')
                ->ofSearch($request)
                ->orderBy('id')
                ->paginate(20);
            $request->flashOnly(['barcode', 'keyword', 'barcode_scan']);
        }
        
        if ($participants->count() == 1) {
            return redirect('/participants/' .$participants->first()->id. '/insert_to_pool/');
        }

        $focus = $this->focus_field($request);
        $checkout = $this->can_checkout($request);
        return view('participant.index', compact('participants', 'focus', 'checkout'));
    }

    protected function focus_field($request)
    {
        if (\Route::currentRouteName() == 'index' && $request->cookie('pool')) {
            $focus = 'barcode_scan';
        } else if ($request->has('keyword')) {
            $focus = 'keyword';        
        } else if ($request->has('barcode')) {
            $focus = 'barcode';        
        } else {
            $focus = 'barcode';            
        }
        return $focus;
    }

    protected function can_checkout($request)
    {
        $pools = collect(is_array($request->cookie('pool')) ? $request->cookie('pool') : []);
        $scans = collect(is_array($request->cookie('scanned')) ? $request->cookie('scanned') : []);    
        
        if ($pools->diff($scans->toArray())->count()) {
            return false;
        } else {
            return true;
        }        
    }

    public function reset()
    {
        request()->session()->forget(['barcode', 'keyword']);
        return redirect('participants');
    }

    public function scan(Request $request)
    {
        $data = (is_array($request->cookie('scanned')) ? $request->cookie('scanned') : []);
        $error = [];
        foreach ($request->barcode_scan as $id => $barcode) {
            if ($barcode) {
                $participant = Participant::find($id, ['id', 'bib_no']);            
                if ($participant->id) {
                    if ($participant->bib_no == $barcode) {
                        array_push($data, $id);
                    }  else {
                        array_push($error, 'You have scanned <strong>' . $barcode . '</strong> for the bib ' . $participant->bib_no);
                    }       
                }
            }
        }
        //dd($request->barcode_scan);
        return back()->with('error', $error)->withCookie(cookie('scanned', $data, 60));
    }

    public function insert_to_pool(Request $request, Participant $participant)
    {
        $data = (is_array($request->cookie('pool')) ? $request->cookie('pool') : []);
        $participant->selected = 1;
        $participant->save();
        array_push($data, $participant->id);
        return redirect('/participants')->withCookie(cookie('pool', $data, 60));
    }

    public function remove_from_pool(Request $request, Participant $participant)
    {
        $data = (is_array($request->cookie('pool')) ? $request->cookie('pool') : []);
        unset($data[array_search($participant->id, $data)]);
        return redirect('/participants')->withCookie(cookie('pool', $data, 60));
    }

    public function checkout(Request $request)
    {
        if (!$this->can_checkout($request)) {
            return back()->with('error', ['Please scan all bib to proceed for checkout.']);
        }
        
        $participants = $this->index($request)->participants;
        if ($participants->isEmpty()) {
            return redirect('/participants');
        }
        return view('participant.checkout', compact('participants'));   
    }

    public function store(Request $request)
    {
        //dd($request);
        $data = $request->only('collecter_id', 'ob_name', 'ob_phone', 'remarks');
        $data['from_ip'] = $request->ip();
        $data['datetime'] = date('Y-m-d H:i:s');
        $data['batch'] = time();

        $participants = $this->index($request)->participants;
        foreach ($participants as $participant) {  
            if ($participant->repc) {
                $participant->repc()->update($data);
            } else {
                $participant->repc()->create($data);
            }
        }

        $cookie1 = \Cookie::forget('pool');
        $cookie2 = \Cookie::forget('scanned');
        return redirect('/participants')->withCookie($cookie1)->withCookie($cookie2);
    }
}

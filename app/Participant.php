<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $fillable = [
            
    ];

	public function repc()
	{
        return $this->hasOne('App\Repc');		
	}

	public function scopeOfSearch($query, $request)
    {
        $barcode = $request->barcode;
        $keyword = $request->keyword;
        $group_code = $request->group_code;
        $team_code = $request->team_code;

        $query
        ->when($barcode, function ($query2) use ($barcode) {
            return $query2->where('barcode', 'like', '%' . $barcode . '%');
        })
        ->when($group_code, function ($query2) use ($group_code) {
            return $query2->where('group_code', 'like', '%' . $group_code . '%');
        })
        ->when($team_code, function ($query2) use ($team_code) {
            return $query2->where('team_code', 'like', '%' . $team_code . '%');
        })
        ->when($keyword, function ($query2) use ($keyword) {
    		return $query2
                ->orWhereRaw('CONCAT(first_name, " ", last_name) LIKE "%'.$keyword.'%"')
                ->orWhere('group', 'like', '%' . $keyword . '%')
                ->orWhere('team', 'like', '%' . $keyword . '%')
                ->orWhere('nric_passport', 'like', '%' . $keyword . '%')
                ->orWhere('bib_no', 'like', '%' . $keyword . '%')
				->orWhere('email', 'like', '%' . $keyword . '%');
		});
        return $query;
    }    
}

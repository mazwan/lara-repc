<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repc extends Model
{
   protected $guarded = [
		'participant_id'
   ];

   public function participant()
   {
   		return $this->belongsTo('App\Repc');
   }
}

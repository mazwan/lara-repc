<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ParticipantController@index');
Route::get('/participants', 'ParticipantController@index')->name('index');
Route::get('/participants/search', 'ParticipantController@search');
Route::get('/participants/reset', 'ParticipantController@reset');
Route::post('/participants/scan', 'ParticipantController@scan');
Route::post('/participants/store', 'ParticipantController@store');
Route::get('/participants/{participant}/insert_to_pool', 'ParticipantController@insert_to_pool');
Route::get('/participants/{participant}/remove_from_pool', 'ParticipantController@remove_from_pool');
Route::get('/participants/checkout', 'ParticipantController@checkout');
Auth::routes();
Route::get('/home', 'HomeController@index');

Route::resources([
    'messages' => 'MessageController'
]);

// Display all SQL executed in Eloquent
/*\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
    var_dump($query->sql);
    var_dump($query->bindings);
    var_dump($query->time);
});*/
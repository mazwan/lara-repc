<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Repc;

class RepcsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Repc::truncate();

        foreach (range(1,300) as $i) {
        	Repc::create([
        		'participant_id' => $faker->unique()->numberBetween(1000, 1999),
        		'scanned' => 1,
        		'collected' => 1,
        		'datetime' => $datetime = $faker->dateTimeBetween('-3 days', '-2 minutes'),        		
        		'created_at' => $datetime,        		
        		'from_ip' => $faker->ipv4,
        		'ob_name' => $faker->name,
        		'ob_nric_passport' => $faker->randomNumber,
        		'ob_phone' => $faker->randomNumber,
        		'start_time' => $faker->dateTimeBetween($datetime),
        		'end_time' => $faker->dateTimeBetween($datetime),
        		'red_flag' => 0,
        		'bulk' => 0,
        		'auth_form' => 0,
        		'batch' => $faker->word,
        		'remarks' => 'remark',
        	]);
        }
    }
}

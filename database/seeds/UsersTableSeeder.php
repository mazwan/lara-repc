<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        User::truncate();

        //admin
        User::create([
        	'id' => 100,
        	'name' => 'Help Desk',
	        'email' => 'helpdesk',
	        'password' => bcrypt('helpdesk'),
	        'remember_token' => str_random(10),
        ]);

        //staff
        User::create([
        	'id' => 200,
        	'name' => 'Counter Staff',
	        'email' => 'staff',
	        'password' => bcrypt('staff'),
	        'remember_token' => str_random(10),
        ]);
    }
}

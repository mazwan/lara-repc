<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Participant;
use App\Repc;

class ParticipantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();        
        //$faker->addProvider(new Faker\Provider\ar_SA\Person($faker));

        Participant::truncate();
        foreach (range(1000,1999) as $i) {

            $group = $group_code = $team = $team_code = null;
            
            if ($i > 999  && $i <= 1010) {
                $group = 'Elite Runners';
                $group_code = 'G001';
            }
            if ($i > 999  && $i <= 1003) {
                $team = 'Team SWAT';
                $team_code = 'T001';
            }
            if ($i > 1010  && $i <= 1013) {
                $team = 'Team Rangers';
                $team_code = 'T002';
            }

        	Participant::create([
        		'id' => $i,
        		'barcode' => $faker->unique()->isbn13,   		
        		'category1' => $faker->randomElement(['Public', 'Corporate']),        		
        		'category2' => $faker->randomElement(['10km Men', '10km Women', '3km Individual', '3km Corporate', '3km Team']),        		
                'shirt1' => $faker->randomElement(['XS Adult', 'S Adult', 'M Adult', 'L Adult', 'XL Adult', 'XS Kids', 'S Kids', 'M Kids', 'L Kids', 'XL Kids']),               
        		'shirt2' => $faker->randomElement(['XS Adult', 'S Adult', 'M Adult', 'L Adult', 'XL Adult', 'XS Kids', 'S Kids', 'M Kids', 'L Kids', 'XL Kids']),        		
                'team' => $team,
                'team_code' => $team_code,
                'group' => $group,
                'group_code' => $group_code,
                'first_name' => $faker->firstName,
        		'last_name' => $faker->lastName,
        		'nric_passport' => $faker->unique()->bankAccountNumber,
        		'gender' => $faker->randomElement(['Male', 'Female']),        		
        		'date_of_birth' => $faker->dateTimeBetween('-70 years', '-5 years'),        		
        		'nationality' => 'Malaysia',        		
        		'country' => 'Malaysia',        		
				'email' => $faker->unique()->email,
				'phone' => $faker->e164PhoneNumber,
				'bib_no' => $bib = $faker->randomElement(['A', 'B', 'C', 'D']) . $faker->unique()->numberBetween(1000, 9999),      		
				'bib_barcode' => $bib,
				'var1' => $faker->sentence,
        	]);
        }

        /*$participant = new Participant;
        $participant->id = 1000;
        $participant->team = 'Team X';
        $participant->team_code = 'E001';
        $participant->update();
        */

        /*foreach (range(1000,1010) as $i) {
            Participant::create([
                'id' => $i,
                'team' => 'Team Name ' . $i,
                'team_code' => $i,
            ]);
        }*/
    }
}

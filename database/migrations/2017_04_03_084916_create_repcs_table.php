<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repcs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('participant_id');
            $table->integer('collecter_id')->nullable();
            $table->boolean('scanned')->default(0);
            $table->boolean('collected')->default(0);
            $table->timestamp('datetime')->nullable();
            $table->string('from_ip')->nullable();
            $table->string('ob_name')->nullable();
            $table->string('ob_nric_passport')->nullable();
            $table->string('ob_phone')->nullable();
            $table->timestamp('start_time')->nullable();
            $table->timestamp('end_time')->nullable();
            $table->boolean('red_flag')->default(0);
            $table->boolean('bulk')->default(0);
            $table->string('batch')->nullable();
            $table->boolean('auth_form')->default(0);
            $table->text('remarks')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repcs');
    }
}

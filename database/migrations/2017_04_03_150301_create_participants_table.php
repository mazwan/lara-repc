<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('selected')->default(0);            
            $table->integer('sorting')->default(0);            
            $table->string('batch')->default('batch1');
            $table->string('parent_id')->nullable();
            $table->string('barcode')->nullable();
            $table->string('category1')->nullable();
            $table->string('category2')->nullable();            
            $table->string('shirt1')->nullable();
            $table->string('shirt2')->nullable();
            $table->string('team')->nullable();
            $table->string('team_code')->nullable();
            $table->string('group')->nullable();
            $table->string('group_code')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('nric_passport')->nullable();            
            $table->string('gender', 10)->nullable();
            $table->date('date_of_birth')->nullable();            
            $table->string('nationality')->nullable();
            $table->string('country')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            /*
            $table->string('blood_type')->nullable();
            $table->string('medical_conditions')->nullable();
            $table->string('allergies')->nullable();
            $table->string('emergency_name')->nullable();
            $table->string('emergency_contact')->nullable();
            $table->string('emergency_relation')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address_postcode')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->string('address_country')->nullable();            
            */
            $table->string('bib_no')->nullable();
            $table->string('bib_barcode')->nullable();
            $table->string('tag_no')->nullable();
            $table->string('tag_barcode')->nullable();
            $table->string('var1')->nullable();
            $table->string('var2')->nullable();
            $table->string('var3')->nullable();
            $table->string('var4')->nullable();
            $table->string('var5')->nullable();
            $table->string('var6')->nullable();
            $table->string('var7')->nullable();
            $table->string('var8')->nullable();
            $table->string('var9')->nullable();
            $table->string('var10')->nullable();
            
            $table->index('batch');
            $table->index('category1');
            $table->index('category2');
            $table->index('shirt1');
            $table->index('shirt2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}

@extends('layouts.app')

@section('pageTitle', 'Home')

@section('content')
<div class="container">
    <div class="row">
    
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">

	                <div class="row">
	                	<div class="col-md-6">
		                	<table class="table table-condensed"> 
		                        <thead> 
		                        <tr> 
		                            <th width="5">&nbsp;</th> 
		                            <th>Name</th> 
		                            <th>Size</th> 
		                            <th>Bib</th> 
		                            <th width="5">&nbsp</th> 
		                        </tr> 
		                        </thead> 
		                        <tbody>
		                        @foreach ($participants as $index => $participant) 
		                        <tr>
		                            <td scope="row">{{ $participants->firstItem() + $index }}</td> 
		                            <td><div class="text-uppercase"><strong>{{ $participant->first_name}} {{ $participant->last_name }}</strong></div></td> 
		                            <td><div>{{ $participant->shirt1 }}</div></td>                             
		                            <td><div class="text-uppercase"><strong>{{ $participant->bib_no }}</strong></div></td> 
		                            <td><a href="#" class="selectMe" data-id="{{ $participant->id }}" data-name="{{ $participant->first_name}} {{ $participant->last_name }}" data-phone="{{ $participant->phone }}"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></td>
		                            
		                        </tr>
		                        @endforeach                         
		                        </tbody> 
		                    </table>
	                    </div>

	                    <div class="col-md-offset-1 col-md-4">
	                    	<h2>Collect by</h2>

	                    	@if ($participants->count() == 1)
	                    		<a href="/participants/checkout" class="btn btn-success">Collect by Owner</a>
	                    	@endif
	                    	
	                    	<hr>
	                    	<form class="" role="form" method="POST" action="{{ url('/participants/store') }}">  
	                    	  {{ csrf_field() }} 
	                    	  <input type="hidden" class="form-control" name="collecter_id" id="collecter_id">                                     
							  <div class="form-group">
							    <label for="name">Name</label>
							    <input type="text" class="form-control" name="ob_name" id="ob_name" required="required" autofocus="autofocus">
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Phone</label>
							    <input type="text" class="form-control" name="ob_phone" id="ob_phone" required="required">
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Remarks</label>
							    <textarea class="form-control" name="remarks" rows="3"></textarea>
							    
							  </div>
							  <div class="checkbox">
							    <label>
							      <input type="checkbox"> Authorization form
							    </label>
							  </div>
							  <button type="submit" class="btn btn-primary">Submit</button>							  
							  <button type="reset" class="btn btn-danger">Reset</button>							  
							</form>

	                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
    	$('.selectMe').click(function(){
    		$('#collecter_id').val($(this).attr('data-id'));
    		$('#ob_name').val($(this).attr('data-name'));
    		$('#ob_phone').val($(this).attr('data-phone'));
    		return false;
    	})        
    })
</script>
@endsection
@extends('layouts.app')

@section('pageTitle', 'Home')

@section('content')
<div class="container">
    <?php 
    echo url("/posts/22"); 
    echo "<br>";
    echo url()->current();
    echo "<br>";
    echo url()->full();
    echo "<br>";
    echo url()->previous();
    echo "<br>";
    echo URL::current();
    echo "<br>";
    
    echo "<br>";
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form" method="GET" action="{{ url('/participants/search') }}">
                      <input name="search" value="1" type="hidden" />  
                      <div class="form-group" style="margin-right:.5em;">
                        <label class="sr-only" for="exampleInputEmail3">Barcode</label>
                        <input name="barcode" class="form-control" id="inputBarcode" value="{{ old('barcode') }}" placeholder="Barcode">
                      </div>
                      <div class="form-group" style="margin-right:.5em;">
                        <label class="sr-only" for="exampleInputPassword3">Password</label>
                        <input name="keyword" style="width:300px;" class="form-control" id="inputKeyword" value="{{ old('keyword') }}" placeholder="Keyword">
                      </div>
                      <button type="submit" class="btn btn-primary">Search</button>
                      @if (request()->has('search'))
                        <a href="{{ url('/participants/reset') }}" class="btn btn-danger">Reset</a>
                      @endif
                    </form>
                </div>
            </div>

            @if ($participants->count())
            <div class="panel panel-default">
                <div class="panel-body">

                    <?php $error = (session('error') ? session('error') : []); ?>
                    @foreach ($error as $msg)
                        <div class="alert alert-danger" style="text-align:center;">{!! $msg !!}</div>
                    @endforeach
                    
                    <form class="" role="form" method="POST" action="{{ url('/participants/scan') }}">                                        
                    <table class="table table-condensed" border="0"> 
                        <thead> 
                        <tr> 
                            <th width="5">&nbsp;</th> 
                            <th width="200">Registration</th> 
                            <th>Group</th>
                            <th>Participant</th> 
                            <th width="80">Bib</th> 
                            <th width="110" style="text-align:center;">&nbsp;</th> 
                            <th width="220" style="text-align:right;"><small class="badge">{{$participants->total()}} {{ str_plural('record', $participants->total()) }}</small></th> 
                        </tr> 
                        </thead> 
                        <tbody>
                        @foreach ($participants as $index => $participant)
                        <?php $remove = 0; ?>
                        
                        @if (request()->has('search') && request()->cookie('pool') && in_array($participant->id, request()->cookie('pool')))
                            <tr class="success">
                        @else 
                            <tr>
                        @endif
                            <td scope="row">{{ $participants->firstItem() + $index }}</td> 
                            <td>
                                <div><strong>{{ $participant->barcode }}</strong></div>
                                <div class="text-info"><small>{{ str_limit($participant->category1, 39) }} - {{ str_limit($participant->category2, 39) }}</small></div>
                                <div><small>#{{ $participant->id }}</small></div>
                            </td> 
                            <td>
                                <div><strong><a href="/participants/search?search=1&group_code={{ $participant->group_code }}">{{ $participant->group}}</a></strong></div>
                                <div><u><a href="/participants/search?search=1&team_code={{ $participant->team_code }}">{{ $participant->team}}</a></u></div>
                            </td> 
                            <td>
                                <div class="text-uppercase"><strong>{{ $participant->first_name}} {{ $participant->last_name }}</strong></div>
                                <div class="text-uppercase">{{ $participant->nric_passport }}</div>
                                <div><small>{{ title_case($participant->gender) }}</small></div>
                            </td> 
                            <td>
                                <div style="margin:6px 0;"><span style="font-size:20px; letter-spacing:1px;" class="label label-default">{{ $participant->bib_no }}</span></div>
                                <div><strong>{{ $participant->shirt1 }}</strong></div>
                            </td> 
                            <td style="text-align:center;">
                                <div class="form-group">
                                    {{-- <bib-scan></bib-scan> --}}

                                    @if ($participant->repc)
                                        <div><small>Collected</small></div>                                    
                                        <small>{{ title_case($participant->repc->created_at->diffForHumans()) }}</small>                                    
                              
                                    @else   
                                        @if (request()->has('search'))
                                            @if (request()->cookie('pool') && in_array($participant->id, request()->cookie('pool')))
                                                <div style="color:green"><small>Selected</small></div>                                    
                                            @else 
                                                <a href="/participants/{{ $participant-> id }}/insert_to_pool" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> add</a>
                                            @endif

                                        @else 
                                            @if (request()->cookie('scanned') && in_array($participant->id, request()->cookie('scanned')))
                                                <center style="color:green; margin-top:1em;"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></center>
                                            @else 
                                                <?php $remove = 1; ?>
                                                <input name="barcode_scan[{{ $participant->id }}]" id="inputBarcodeScan{{ $participant->id }}" class="form-control input barcodeScan" type="text" placeholder="Scan" tabindex="{{ $index }}" />
                                            @endif
                                        @endif                                        
                                    @endif    

                                </div>
                            </td> 
                            <td>
                                @if ($remove == 1)
                                    <a class="pull-right" style="color:maroon;" href="/participants/{{ $participant-> id }}/remove_from_pool"><small><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></small></a>
                                @endif
                            </td> 
                        </tr>
                        @endforeach                         
                        </tbody> 
                    </table>
                    <hr>

                    @if ($checkout === true)
                    <div class="pull-right">
                        <a href="/participants/checkout" class="btn btn-success">Checkout</a>                    
                        <a href="/participants/checkout" class="btn btn-success">Bulk Checkout</a>                    
                    </div>
                    @endif

                    <input type="submit" hidden />
                    </form>
                    {{ $participants->appends(request()->input())->links() }}                    
                    
                </div>
            </div>
            
            @else
                <div class="row">                   
                </div>
            @endif

        </div>
        
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {

        @if ($focus == 'barcode')
            $('#inputBarcode').select().focus();    
        @elseif ($focus == 'keyword')
            $('#inputBarcode').val('');    
            $('#inputKeyword').select().focus();
        @elseif ($focus == 'barcode_scan')
            $('.barcodeScan').first().select().focus();
        @endif
        //$('.scanBarcode').on('click');
    })
</script>
@endsection